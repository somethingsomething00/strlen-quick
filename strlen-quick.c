#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <stdint.h>

#include "./config.h"

#define Megabytes(v_) ((v_) * 1024U * 1024U)


#if defined (__clang__) || defined (__GNUC__)

#define NOINLINE __attribute__((noinline))
#include <immintrin.h>
#include <x86intrin.h>

#elif defined (_WIN32)

#define NOINLINE __declspec(noinline)
#include <windows.h>
#include <intrin.h>

#else
/* Other compilers can go here */
#define NOINLINE

#endif


#ifdef _WIN32
uint64_t get_platform_time()
{
	LARGE_INTEGER result;
	GetPerformanceCounter(&result);
	return result.QuadPart;
}

double get_platform_time_in_seconds(uint64_t n)
{
	double result;
	result =  (double)n / (double)GetPerformanceFrequency().QuadPart;
	return result;
}

int bit_scan_forward_32(int n)
{
	return _BitScanForward(n);
}

#else

uint64_t get_platform_time()
{
	uint64_t result;
	struct timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	result =  (t.tv_sec * (uint64_t)1E9) + t.tv_nsec;
	return result;
}

double get_platform_time_in_seconds(uint64_t n)
{
	return (double)n / 1E9;
}

int bit_scan_forward_32(int n)
{
	return _bit_scan_forward(n);
}

#endif /* _WIN32 */


#if USE_AVX
// Make sure the compiler isn't swapping us out for its own strlen
NOINLINE
int strlen_avx(const char *s)
{
	__m256 string256;
	__m256 zero;
	__m256i ff;
	__m256i compare;
	int mask;
	const char *bp;
	const char *pScan;

	bp = s;
	mask = 0;
	zero = _mm256_setzero_ps();
	ff = _mm256_set1_epi8(0xff);

	for(;;)
	{
		_mm_prefetch(s + 128, _MM_HINT_NTA);
		string256 = _mm256_loadu_ps((void *)s);
		compare = _mm256_castps_si256(_mm256_cmp_ps(string256, zero, _CMP_EQ_OQ));
		mask = _mm256_testz_si256(compare, ff);
		if(!mask)
		{
			// mask = _mm256_movemask_epi8(compare); // No avx2 :(
			break;
		}
		s += (256 / 8);
	}
	// Since we don't have avx2 we need to scan the string manually once again to find the null byte
	// I didn't spend too much time thinking of a better way
	if(s - bp < (256 / 8)) pScan = bp;
	else pScan = s - (256 / 8);
	while(*pScan) pScan++;
	return (pScan - bp);
}
#endif /* USE_AVX */

#if USE_SSE2_MIXED

// More or less like sse2
// Uses the _mm_test_all_zeros intrinsic instead of movemask except at the end

// Make sure the compiler isn't swapping us out for its own strlen
NOINLINE
int strlen_sse2_mixed(const char *s)
{
	__m128i string128;
	__m128i zero;
	__m128i ff;
	__m128i compare;
	int mask;
	const char *bp;
	int lowestBit;

	bp = s;
	mask = 0;
	zero = _mm_setzero_si128();
	ff = _mm_set1_epi8(0xFF);

	for(;;)
	{
		/* Sometimes prefeteching helps, sometimes it makes things worse.
		  On my machine for this function in particular it helped by about 100 - 200 MiB/s */
		_mm_prefetch(s + 128, _MM_HINT_NTA);
		string128 = _mm_loadu_si128((__m128i *)s);
		compare = _mm_cmpeq_epi8(string128, zero);
		mask = _mm_test_all_zeros(compare, ff);
		if(!mask)
		{
			mask = _mm_movemask_epi8(compare);
			break;
		}
		s += (128 / 8);
	}
	lowestBit = bit_scan_forward_32(mask);
	return (s - bp + lowestBit);
}
#endif /* USE_SSE2_MIXED */


#if USE_SSE2
// Make sure the compiler isn't swapping us out for its own strlen
NOINLINE
int strlen_sse2(const char *s)
{
	__m128i string128;
	__m128i zero;
	__m128i compare;
	int mask;
	const char *bp;
	int lowestBit;

	bp = s;
	mask = 0;
	lowestBit = 0;
	zero = _mm_setzero_si128();

	for(;;)
	{
		_mm_prefetch(s + 128, _MM_HINT_NTA);
		string128 = _mm_loadu_si128((__m128i *)s);
		compare = _mm_cmpeq_epi8(string128, zero);
		mask = _mm_movemask_epi8(compare);
		if(mask) break;
		s += (128 / 8);
	}

	lowestBit = bit_scan_forward_32(mask);
	return (s - bp + lowestBit);
}
#endif /* USE_SSE2 */

int main()
{
	char *buffer = NULL;
	volatile int result;
	unsigned int toAlloc;
	double seconds;
	uint64_t t1, t2;

	toAlloc = Megabytes(64);
	buffer = malloc(toAlloc * sizeof *buffer + 1);
	assert(buffer);
	memset(buffer, 'a', toAlloc - 1);
	buffer[toAlloc] = 0;

	t1 = get_platform_time();
	result = strlen_sse2(buffer);
	// result = strlen_sse2_mixed(buffer);
	// result = strlen_avx(buffer);
	// result = strlen(buffer);
	t2 = get_platform_time();
	seconds = get_platform_time_in_seconds(t2 - t1);

	printf("len: %d\n", result);
	printf("%.4f MiB / sec  (%f sec)\n", (double)toAlloc / (double)(1 << 20) / seconds, seconds);
	free(buffer);
	return 0;
}
