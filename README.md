# strlen-quick

Minimal SSE2 and AVX implementations of strlen.

They seem to rival clang's and gcc's own versions, which is nice.

This code open to anyone, feel free to grab it for your own use.


Do note that most instrinsic instructions are strict about memory alignment. I didn't include any code for that in these examples, but AVX instructions expect to be aligned on 32 byte addresses, and SSE2 on 16 respectively.

For example, doing something like this will surely result in a crash:

```c
char *buffer;
buffer = malloc(1024);
buffer++;
strlen_avx(buffer);
//Oops!
```
You do get `_mm_loadu` and `_mm_storeu` instructions for unaligned access, but not all intrinsics work with unaligned memory. Be warned :)

## Dependencies
A cpu that supports at least the SSE2 instruction set.
More recent instruction sets can be disabled at compile time in [config.h](./config.h)

## To build
```console
./build.sh
```

## To run
```console
./strlen-quick
```
