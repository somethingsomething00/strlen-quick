#ifndef _CONFIG_H_
#define _CONFIG_H_

#define USE_SSE2 1
#define USE_SSE2_MIXED 1
#define USE_AVX 1

#endif /* _CONFIG_H_ */
