#!/bin/sh
cc="gcc"
src="strlen-quick.c"
mflags="-msse2 -msse4.2 -mavx"
cflags="-O2"
bin="strlen-quick"
warn="-Wall"

compile_command="$cc $src -o $bin $cflags $mflags  $warn"
echo $compile_command
$compile_command
